//
//  NewsListTblCell.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow

class NewsListTblCell: UITableViewCell {

    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblNewsTitle: UILabel!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblDatetime: UILabel!
    var fromVC :NewsListViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgNews.isUserInteractionEnabled = true
        let gestureUSPS = UITapGestureRecognizer(target: self, action: #selector(viewFullImage(_:)))
        imgNews.addGestureRecognizer(gestureUSPS)
    }
    
    func setCellVaue(data:Article){

        if let imgUrl = data.urlToImage{
            imgNews.sd_setImage(with: URL(string:(imgUrl)), placeholderImage:#imageLiteral(resourceName: "placeHolder"))
        }else{
         imgNews.image = #imageLiteral(resourceName: "placeHolder")
        }
        lblNewsTitle.text = data.title
        lblAuthorName.text = data.author
        lblDatetime.text = data.publishedAt
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    @objc func viewFullImage(_ gesture : UITapGestureRecognizer){
        let fullscreen = FullScreenSlideshowViewController()
        let image = ImageSource(image: (imgNews.image)!)
        fullscreen.inputs = [image]
        fromVC!.present(fullscreen, animated: true, completion: nil)
    }
}
