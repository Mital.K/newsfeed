//
//  APIManagar.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import Foundation
import Alamofire

class ApiCallManager {
    
    static let shared = ApiCallManager()
    private init() {  }

    func makeAPICall(with endPoint: String, method: HTTPMethod, params: Parameters?, encoding:  ParameterEncoding, headers: HTTPHeaders?,  completion: @escaping (Error?, NewsData?,Int?) -> Void) {
        
        AF.request(endPoint, method: method, parameters: params, encoding: encoding , headers: headers)
            
            .response { (response) in
               
                if let error = response.error {
                    print("Error making an API call. - \(error.localizedDescription)")
                    completion(error, nil, nil)
                    return
                }
                if let data = response.data {
                    do {
                        let model = try JSONDecoder().decode(NewsData.self, from: data)
                      
                         do {
                         let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? AnyObject
                         
                            print("jsonResult = \(String(describing: jsonResult))")
                         
                         }catch {
                         print("post data error",error)
                         }
                        
                        completion(nil, model, nil)
                    } catch let error {
                        guard let status = response.response?.statusCode else{return}
                        if status == 500{
                            print("internal server error")
                        }
                        print("error status code is:\(String(describing: response.response?.statusCode))")
                        print("Error decoding - \(error.localizedDescription)")
                        completion(error, nil, status)
                    }
                    return
                }
        }
        
    }
}
