//
//  BaseModel.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import Foundation

class BaseModel: Decodable {
    
    var errorCode: Int!
    var message: String!
    var data: String? = nil
    
    class func resolveDataInResponse<T:Decodable>(data:String,toModel:T.Type)->T? {
        let jsonDecoder = JSONDecoder()
        var userDataStruct:T? = nil
        
        do{
            let responseData: Data? = data.data(using: .utf8)
            userDataStruct = try jsonDecoder.decode(toModel, from:  responseData!)
            //  userDataStruct.
            // print("user data:\(userDataStruct)")
            
        }catch{
            print("error")
            print(error.localizedDescription)
        }
        return userDataStruct
    }
    
    class func resolveDataInAraryResponse<T:Decodable>(data:String,toModel:[T].Type)->Array<Any>? {
        let jsonDecoder = JSONDecoder()
        var userDataStruct:Array<Any>?
        
        do{
            let responseData: Data? = data.data(using: .utf8)
            userDataStruct = try jsonDecoder.decode(toModel, from:  responseData!)
            
            //  print("user data:\(userDataStruct)")
            
            
        }catch{
            print(error.localizedDescription)
        }
        return userDataStruct
    }
    
}
