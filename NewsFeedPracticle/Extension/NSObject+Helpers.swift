//
//  NSObject+Helpers.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import Foundation


extension NSObject {
    
    var className : String {
        return String(describing: self)
    }
    
    static var className : String {
        return String(describing: self)
    }
}
extension String{
    var convertDateTimeFormate:String{
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let showDate = inputFormatter.date(from: self)
        inputFormatter.dateFormat = "dd MMM. yyyy hh:mm a"
        let resultString = inputFormatter.string(from: showDate!)
        return resultString
    }
}
