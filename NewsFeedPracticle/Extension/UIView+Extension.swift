//
//  UIView+Extension.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import UIKit
import SystemConfiguration

extension UIViewController {
    
    var checkInternet:Bool{
        
        if isInternetAvailable{
            return true
        }else{
            self.showAlert(appName,NSLocalizedString("No internet connection!", comment: ""))
             return false
        }
    }
    var isInternetAvailable:Bool{
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    func showAlert(_ title : String = appName, _ message:String){
           
           let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
           let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("Ok",comment: ""), style: .cancel) { action -> Void in
               
           }
           alertController.addAction(cancelAction)
           self.present(alertController, animated: true, completion: nil)
       }
}
