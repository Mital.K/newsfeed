//
//  Constant.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import Foundation
var appName : String {
    return Bundle.main.infoDictionary!["CFBundleName"] as! String
}
let AppDelegateObj : AppDelegate = AppDelegate.shared
let BASE_URL = "https://newsapi.org/v2/top-headlines?sources=google-news&apiKey=9a0c8e375ada4198a26f7a52638c4b78"
