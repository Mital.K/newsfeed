//
//  NewsDetailViewController.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgNews: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAuthorName: UILabel!
    @IBOutlet weak var lblWebLink: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    var articleDetail :Article?
    override func viewDidLoad() {
        super.viewDidLoad()
         self.title = "News Details"
        setView()
    }
    func setView(){
        
        lblWebLink.isUserInteractionEnabled = true
        let gestureUSPS = UITapGestureRecognizer(target: self, action:#selector(onClickLabel(_:)))
        lblWebLink.addGestureRecognizer(gestureUSPS)
        
        imgNews.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewFullImage(_:)))
        imgNews.addGestureRecognizer(tapGesture)
        
        lblTitle.text = articleDetail?.title
        if let imgUrl = articleDetail?.urlToImage{
            imgNews.sd_setImage(with: URL(string:(imgUrl)), placeholderImage:#imageLiteral(resourceName: "placeHolder"))
        }else{
           imgNews.image = #imageLiteral(resourceName: "placeHolder")
        }
        lblDescription.text = articleDetail?.content
        lblAuthorName.text = articleDetail?.author
        lblWebLink.text = articleDetail?.url
        lblDateTime.text = articleDetail?.publishedAt
    }
    @objc func onClickLabel(_ gesture : UITapGestureRecognizer) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let webVC = storyboard.instantiateViewController(withIdentifier: NewsWebView.className) as! NewsWebView
         webVC.urlRequest = articleDetail?.url
         self.navigationController?.pushViewController(webVC, animated: true)
        
    }
    @objc func viewFullImage(_ gesture : UITapGestureRecognizer){
        let fullscreen = FullScreenSlideshowViewController()
        let image = ImageSource(image: (imgNews.image)!)
        fullscreen.inputs = [image]
        self.present(fullscreen, animated: true, completion: nil)
    }
}
