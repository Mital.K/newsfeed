//
//  NewsListViewController.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import UIKit
import Alamofire

class NewsListViewController: UIViewController{

    @IBOutlet weak var tblNewsListView: UITableView!
    
    var arrNewsList = [Article]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "News List"
        registerTblCell()
        if checkInternet{
            callListAPI()
        }
        
        // Do any additional setup after loading the view.
    }
    fileprivate func registerTblCell(){
         tblNewsListView.register(UINib(nibName: NewsListTblCell.className, bundle: Bundle.main), forCellReuseIdentifier: NewsListTblCell.className)
    }
    func callListAPI(){
        LoaderView.shared.showOverlay(view: self.view)
        ApiCallManager.shared.makeAPICall(with: BASE_URL, method: .get, params: nil, encoding: JSONEncoding.default, headers: nil) { (error, basemodel, status) in
            
            LoaderView.shared.hideOverlayView()
            if error == nil{
                if basemodel != nil{
                    self.arrNewsList = basemodel?.articles as! [Article]
                    self.tblNewsListView.reloadData()
                }
            }
        }
    }

}
extension NewsListViewController:UITableViewDataSource,UITableViewDelegate{
    //MARK:- Tableview Delegates
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNewsList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsListTblCell.className, for: indexPath) as! NewsListTblCell
            cell.fromVC = self
            cell.setCellVaue(data: arrNewsList[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
           let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
           let detailVC = storyboard.instantiateViewController(withIdentifier: NewsDetailViewController.className) as! NewsDetailViewController
            detailVC.articleDetail = arrNewsList[indexPath.row]
            self.navigationController?.pushViewController(detailVC, animated: true)
        
    }
}
