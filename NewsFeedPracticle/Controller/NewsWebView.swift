//
//  NewsWebView.swift
//  NewsFeedPracticle
//
//  Created by MiThi on 08/11/20.
//  Copyright © 2020 Mital. All rights reserved.
//

import UIKit
import WebKit

class NewsWebView: UIViewController {

    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    var urlRequest : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        webView.navigationDelegate = self
        print(urlRequest)
        // Do any additional setup after loading the view.
    }
    fileprivate func loadWebView(){
        
        if let reqURL = URL(string: self.urlRequest ?? ""){
            
            webView.load(URLRequest(url: reqURL))
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewsWebView : WKNavigationDelegate {

    //MARK:- WKNavigationDelegate
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Start to load")
        activity.startAnimating()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activity.stopAnimating()
    }

}
